from email import message, message_from_binary_file
from ensurepip import bootstrap
import time
import json
import random
from datetime import datetime
from data_generator import generate_message
from kafka import KafkaProducer

def serializer(message):
    return json.dumps(message).encode('utf-8')

# Kafka Producer
producer = KafkaProducer(
    bootstrap_servers=['localhost:9092'],
    value_serializer=serializer
)

if __name__ == '__main__':
    # inifinit loop
    while True:
        # Generate message
        message = generate_message()

        ## send message
        print(f'Producing message at {datetime.now()} | Message = {message}')
        producer.send('message', message)

        ##sleep for  random
        time_to_sleep = random.randint(1,10)
        time.sleep(time_to_sleep)

