# kafka_docker_setup

## Base docker setup

- [Apache Kafka packaged by Bitnami](https://github.com/bitnami/bitnami-docker-kafka)
- [Zoonavigator](https://github.com/elkozmon/zoonavigator)
- [Kafka UI](https://www.youtube.com/watch?v=_qHxwsLcoVQ)

This docker repository is based on `bitnami/zookeeper` and `bitnami/kafka` [official page](https://bitnami.com/stack/kafka/containers).



kafka-topics.sh --create  --bootstrap-server localhost:9092 --topic message --partitions 1 --replication-factor 1

kafka-topics.sh --describe --bootstrap-server localhost:9092  --topic message
## Help

- [How to Connect to Apache Kafka running in Docker(multiple scenarios)](https://www.youtube.com/watch?v=L--VuzFiYrM)
- [channel](https://www.youtube.com/watch?v=4xFZ_iTZLTs)
- [Deeptalk 6](https://www.youtube.com/watch?v=_qHxwsLcoVQ) and githug [repo]()
- [Ready to use production container](https://github.com/conduktor/kafka-stack-docker-compose)

